startGame(8,8,10);

function startGame (WIDTH,HEIGHT,BOMBS_COUNT) {
    const field = document.querySelector('.field'); //  поле
    document.getElementById('mines-count').innerHTML = BOMBS_COUNT;
    const cellsCount =  WIDTH * HEIGHT; // кол-во ячеек
    field.innerHTML = '<button></button>'.repeat(cellsCount); // создание кнопки для каждой ячейки
    const cells = [...field.children]; // складываем все кнопки в один массив

    const bombs = [...Array(cellsCount).keys()] // создаем массив бомб
    .sort (() => Math.random() - 0.5)  // рандомная сортировка 
    .slice (0, BOMBS_COUNT); 

    field.addEventListener ('click', (event) => {
        if (event.target.tagName !== 'BUTTON') { // если клик не по элементу "button"
            return;
        }

        const index = cells.indexOf(event.target); //ищем индекс нажатой кнопки
        const column = index % WIDTH; // получение координат кнопки
        const row = (index - column) / WIDTH;  // получение координат кнопки
        open(row, column);
    });

    field.addEventListener ('click', ({target}) => {
        if (target.matches (".bombs")) { 
            const parent = target.parentNode;
            for (i = 0; i < parent.children.length; i++) {
                parent.children[i].disabled = true;
            }
        }
    });

    function isValid (row, column) { // проверка валидности
        return row >= 0 
            && row < HEIGHT
            && column >= 0
            && column < WIDTH;
    }

    function getCount (row,column) { // функция вычисления - сколько мин хранится рядом

        let count = 0;
        for (let x = -1; x <= 1; x++) { // перебираем соседние ячейки по х
            for (let y = -1; y <= 1; y++) { // перебираем соседние ячейки по у
                if (isBomb(row + y, column + x)) {
                    count++;
                }
            }
        }
        return count;
    }


    function open(row, column) { // функция - действие при открытии ячейки 

        if (!isValid(row, column)) return;
        const index = row * WIDTH + column;
        const cell = cells[index];

        if (cell.disabled === true) return; // если ячейка уже открытая

        cell.disabled = true;

        if (isBomb(row, column)) { // если это бомба 
            cell.setAttribute('class', 'bombs');

            return;
        }
        
        const count = getCount(row, column); 
        
        if (count !== 0) {
            cell.innerHTML = count;
            if (count == 1) {
                cell.setAttribute('class', 'x1');
            }
            if (count == 2) {
                cell.setAttribute('class', 'x2');
            }
            if (count == 3) {
                cell.setAttribute('class', 'x3');
            }
            if (count == 4) {
                cell.setAttribute('class', 'x4');
            }
            if (count == 5) {
                cell.setAttribute('class', 'x5');
            }
            if (count == 6) {
                cell.setAttribute('class', 'x6');
            }
            if (count == 7) {
                cell.setAttribute('class', 'x7');
            }
            if (count == 8) {
                cell.setAttribute('class', 'x8');
            }
            return;            
        }

        for (let x = -1; x <= 1; x++) { // перебираем соседние ячейки по х
            for (let y = -1; y <= 1; y++) { // перебираем соседние ячейки по у
                open(row + y, column + x); // открываем соседние ячейки если count = 0
            }
        }
    }

    function isBomb(row, column) { // функция - проверка - является ли элемент бомбой
        if (!isValid(row, column)) return false;
        const index =  row * WIDTH + column; // индекс кликнутой ячейки 

        return bombs.includes(index);
    }
}

